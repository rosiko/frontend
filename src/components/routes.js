import React from "react";
import { Route, Routes } from "react-router-dom";
import ErrorPage from "../pages/Error";
import Home from "../pages/Home";
import Login from "../pages/Login";
import NewMatch from "../pages/NewMatch";
import JoinMatch from "../pages/JoinMatch";
import WaitingRoom from "../pages/WaitingRoom";
import Match from "../pages/Match";
import ProtectedRoute from "./protectedRoute";
import Register from "../pages/register";
import AccountVerification from "../pages/accountVerification";
import ResetPassword from "../pages/resetPassword";
import ForgotPassword from "../pages/forgotPassword";
import Settings from "../pages/settings";

function AppRoutes() {
    return (
        <Routes>
            <Route path="/" element={<ProtectedRoute><Home/></ProtectedRoute>}/>
            <Route path="/home" element={<ProtectedRoute><Home/></ProtectedRoute>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/forgot-password" element={<ForgotPassword/>}/>
            <Route path="/reset-password" element={<ResetPassword/>}/>
            <Route path="/account-verification" element={<AccountVerification/>}/>
            <Route path="/settings" element={<ProtectedRoute><Settings/></ProtectedRoute>}/>
            <Route path="/new_match" element={<ProtectedRoute><NewMatch/></ProtectedRoute>}/>
            <Route path="/join_match" element={<ProtectedRoute><JoinMatch/></ProtectedRoute>}/>
            <Route path="/waiting_room/:id" element={<ProtectedRoute><WaitingRoom/></ProtectedRoute>}/>
            <Route path="/match/:id" element={<ProtectedRoute><Match/></ProtectedRoute>}/>
        </Routes>
    );
}

export default AppRoutes