import {Navigate} from 'react-router-dom';
import '../styles/Home.css';
import React from 'react';
import {isAuthenticated} from "../js/clients/authenticationClient";

function ProtectedRoute({ children }) {
    return isAuthenticated() ? children : <Navigate to="/login" />;
}

export default ProtectedRoute;