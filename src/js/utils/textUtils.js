export function capitalizeFirstLetter(string) {
    if (!string) return string;
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function lowCapFirstLetter(string) {
    if (!string) return string;
    return string.charAt(0).toLowerCase() + string.slice(1);
}