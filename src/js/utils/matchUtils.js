export function getPlayerById(match, playerId){

    if(playerId !== null && match.players != null){
        for(let player of match.players){
            if(playerId === player.id){
                return player;
            }
        }
    }

    return null;
}

export default getPlayerById;