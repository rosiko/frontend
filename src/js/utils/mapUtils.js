import countDice from "./diceUtils";

export function getTerritoryById(map, territoryId){

    if(territoryId !== null && map.territories != null){
        for(let territory of map.territories){
            if(territoryId === territory.id){
                return territory;
            }
        }
    }

    return null;
}

export default getTerritoryById;