//Ritorna il numero attuale di dadi
export function countDice(diceList){
    let numberOfDice = 0;
    if(diceList !== null) {
        for(let i=0; i<diceList.length; i++){
            if( diceList[i] !== "add" 
                && diceList[i] !== "remove" 
                && diceList[i] !== "none"
                && diceList[i] !== "rolling"
            ) numberOfDice++;
        }
    }
    return numberOfDice;
}

export function getDicesOrdered(dicesResult){
    let dices = ["none", "none", "none"];
    let sortedDices = [];

    if(dicesResult != null){
        sortedDices = dicesResult.sort((a, b) => b - a)
    }

    for(let i=0; i<sortedDices.length; i++){
        dices[i] = sortedDices[i];
    }

    return dices;
}

export default {countDice, getDicesOrdered}