//const endPoint = "http://localhost:8081"; //Local Backend
const endPoint = process.env.BACKEND_URL; //Production Backend

//export const wsEndPoint = "http://localhost:8081/stomp"; //Web socket local endpoint
export const wsEndPoint = process.env.BACKEND_URL+"/stomp"; ///Web socket production endpoint

export default endPoint;