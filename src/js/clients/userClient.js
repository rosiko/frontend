import endPoint from "./endPoint";
import useAxiosInterceptor from "../axiosInterceptor";

const axios = useAxiosInterceptor(true);

export async function getUser(){
    let response = await axios
        .get(endPoint + '/user')
    return response.data
}

export async function updateUser(name, password){
    let payload = {}
    if(name) payload.name = name
    if(password) payload.password = password
    await axios.put(endPoint + '/user', payload)
}

export async function getCurrentMatch(){
    let match = null;

    try{
        let response = await axios.get(endPoint + `/user/current-match`)
        match = response.data;
    }
    catch(error){ console.error(error); }

    return match;
}