import {countDice} from "../utils/diceUtils";
import endPoint from "./endPoint";
import useAxiosInterceptor from "../axiosInterceptor";

const axios = useAxiosInterceptor(true);

export const getPlayer = (match, playerId) => {
    if(!match?.players) return null;

    for(let player of match.players){
        if(player.id === playerId){
            return player
        }
    }

    throw new Error(`No player found with id = ${playerId}`)
}

export async function newMatch( matchName, password ){
    let response = await axios
        .post(endPoint + '/matches', { "name": matchName, "password": password } )
        .catch(error => console.error(error))
    return response.data.id;
}

export async function joinMatch( matchId ){
    await axios
        .put(endPoint + `/matches/${matchId}/join`)
        .catch(error => console.error(error))
}

export async function leavesMatch( matchId ){
    try{
        await axios.put(endPoint + `/matches/${matchId}/leaves`);
        console.log("Match " + matchId + " abandoned");
    }
    catch(error){
        console.error(error);
    }
}

export async function getMatch( matchId ){
    let match = null;

    try{
        let response = await axios.get(endPoint + `/matches/${matchId}`)
        match = response.data;
    }
    catch(error){ console.error(error); }

    return match;
}

export async function getAvailableMatches(){
    let matches = []
    
    try{
        let response = await axios.get(endPoint + '/matches?state=WAITING&state=READY')
        matches = response.data
    }
    catch(error){ console.error(error) }

    return matches;
}

export function startMatch( matchId ){    
    try{
        axios.put(endPoint + `/matches/${matchId}/start`)
    }
    catch(error){
        console.error(error)
    }
}


export async function getMatchStatus(matchId) {
    let status = null;

    if(matchId === null) return null

    try{        
        let response = await axios.get(endPoint + `/matches/${matchId}/status`)
        status = response.data;
    }
    catch(error){ console.error(error); }

    return status;
}

export const getPlayerOnDuty = (match) => {
    return getPlayer(match, match.playerOnDutyId)
}

export function placeArmies( matchId, armiesToPlaceThisTurn, setArmiesToPlaceThisTurn ){    
    try{
        console.log("Player on duty are placing the armies")
        axios.post(endPoint + `/matches/${matchId}/place-armies`, armiesToPlaceThisTurn)
        .then(()=>{ setArmiesToPlaceThisTurn([]) })
    }
    catch(error){ console.error(error) }
}

export async function getUserId(){
    let userId = null

    try{
        let response = await axios.get(endPoint + `/user`)
        userId = response.data.id
    }
    catch(error){ console.error(error); }

    return userId
}

export async function selectAttacker(matchId, territoryId) {
    try{
        await axios.put(
            endPoint + '/api/v1/match/select_attacker',
            {"matchId": matchId, "territoryId": territoryId}
        )
    }
    catch(error) { console.error(error) }
}

export async function selectDefender(matchId, territoryId) {
    try{
        await axios.put(
            endPoint + '/api/v1/match/select_defender',
            {"matchId": matchId, "territoryId": territoryId}
        )
    }
    catch(error){ console.error(error) }
}

const moveArmies = (territoryFrom, territoryTo, armies, setMovedArmies) => {
    if(territoryFrom.placedArmies - armies >= 1 && territoryTo.placedArmies + armies >= 1){
        setMovedArmies(armies);
    }    
}

const attack = (match, attackerDice, setRolling) => {
    let diceNumber = countDice(attackerDice);

    setRolling(true);

    try{
        let payload = {"matchId": match.id, "numberOfAttackerDice": diceNumber}
        axios.post(endPoint + '/api/v1/match/attack', payload)
    }
    catch(error){
        console.error(error)
    }
}

const deselectTerritory = (match, territory) => {
    try{
        let payload = {"matchId": match.id, "territoryId": territory.id}
        axios.put(endPoint + '/api/v1/match/deselect_territory', payload)
    }
    catch(error){
        console.error(error)
    }
}

const selectTerritoryFrom = (match, territory) => {
    try{
        let payload = {"matchId": match.id, "territoryId": territory.id}
        axios.put(endPoint + '/api/v1/match/select_territory_from', payload)
    }
    catch(error) { console.error(error) }
}

const selectTerritoryTo = (match, territory) => {
    try{
        let payload = {"matchId": match.id, "territoryId": territory.id}
        axios.put(endPoint + '/api/v1/match/select_territory_to', payload)
    }
    catch(error) { console.error(error) }
}

//Ritorna il numero corretto di armate
const getNumberOfArmies = (match, territory, placedArmiesSet, movedArmies) => {

    if( territory === null || territory === undefined ) return 0;

    let armies = territory.placedArmies;
    let territoryId = territory.id;
    let territoryFromId = match.armiesMovement.territoryFromId;
    let territoryToId = match.armiesMovement.territoryToId;

    if(placedArmiesSet !== undefined){
        for(let placedArmies of placedArmiesSet){
            if(placedArmies.territoryId === territoryId){
                armies += placedArmies.quantity;
                break;
            }
        }
    }

    if(territoryFromId !== null && territoryToId !== null && territoryFromId === territoryId) armies -= movedArmies;
    if(territoryFromId !== null && territoryToId !== null && territoryToId === territoryId) armies += movedArmies;
    
    return armies;
}

const endsAttacks = (match) => {
    try{
        let payload = {"matchId": match.id}
        axios.put(endPoint + '/api/v1/match/displacement_stage', payload)
    }
    catch(error) { console.error(error) }
}

const endsTurn = (match) => {
    try{
        let payload = {"matchId": match.id}
        axios.post(endPoint + '/api/v1/match/ends_turn', payload)
    }
    catch(error) { console.error(error) }
}

const playCards = ( match, player, cardSet) => {
    try{
        let payload = [ cardSet[0].id, cardSet[1].id, cardSet[2].id ]
        axios.post(endPoint + `/matches/${match.id}/play-cards`, payload)
    }
    catch(error) { console.error(error) }
}

const confirmMove = (match, movedArmies, setMovedArmies) => {
    try{
        let json = {"matchId": match.id, "territoryFrom": match.armiesMovement.territoryFromId, "territoryTo": match.armiesMovement.territoryToId, "movedArmies": movedArmies}
        axios.put(endPoint + '/api/v1/match/confirm_move', json).then(()=>{setMovedArmies(0)})
    }
    catch(error) { console.error(error) }
}

const trisBonusCalculator = (player, cards, map) =>{
    let type1 = cards[0].cardType;
    let type2 = cards[1].cardType;
    let type3 = cards[2].cardType;
    let bonus = 0;
    
    //Bonus dovuto alle carte
    if(type1 === "TRACTOR" && type2 === "TRACTOR" && type3 === "TRACTOR" ){ bonus = 4 }
    if(type1 === "FARMER" && type2 === "FARMER" && type3 === "FARMER" ){ bonus = 6 }
    if(type1 === "COW" && type2 === "COW" && type3 === "COW" ){ bonus = 8 }
    if( (type1 !== "JOLLY" && type1 !== type2 && type1 !== type3) &&
        (type2 !== "JOLLY" && type2 !== type3 && type2 !== type1) &&
        (type3 !== "JOLLY" && type3 !== type1 && type3 !== type2)
    ){ bonus = 10 }
    if( (type1 === "JOLLY" && type2 === type3 && type2!== "JOLLY")||
        (type2 === "JOLLY" && type1 === type3 && type1!== "JOLLY")||
        (type3 === "JOLLY" && type1 === type2 && type1!== "JOLLY")
    ){ bonus = 12 }

    //Bonus dovuto ai territori posseduti
    if(bonus > 0){
        let territories = map.territories;
        
        for(let j=0; j<cards.length; j++){
            for(let i=0; i<territories.length; i++){                
                if(territories[i].id === cards[j].territoryId && territories[i].ownerId === player.id){
                    bonus += 2;
                    i=territories.length;
                }
            }
        }
    }

    return bonus;
}

export default {
    moveArmies,
    attack,
    deselectTerritory,
    selectTerritoryFrom,
    selectTerritoryTo,
    getNumberOfArmies,
    endsTurn,
    endsAttacks,
    playCards,
    confirmMove,
    trisBonusCalculator,
    leavesMatch
};