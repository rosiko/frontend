import endPoint from "./endPoint";
import useAxiosInterceptor from "../axiosInterceptor";

const axios = useAxiosInterceptor(false);

export const login = async (email, pass) => {

    const loginPayload = {
        email: email,
        password: pass
    }

    await axios.post(endPoint + "/auth/login", loginPayload)
        .then(response => {
            const token  =  response.data.token;
            localStorage.setItem("token", token);
            console.log("Logged in successfully")
        })
}

export const logout = () => {
    localStorage.removeItem("token")
}

export const register = async (name, email, pass) => {

    const payload = {
        name: name,
        email: email,
        password: pass
    }

    await axios.post(endPoint + "/auth/register", payload)
        .then(() => {
            console.log("Registered successfully")
        })
        .catch(ex => {
            console.dir(ex)
            let message = "Registration failed"
            if(ex.response?.data?.message){
                message += ", " + ex.response.data.message
                throw new Error(message)
            }
            else throw ex
        })
}

export const initiateResetPassword = async (email) => {

    let query = {email: email}

    await axios.post(endPoint + "/auth/initiate-reset-password", null, {params: query})
        .catch(ex => {
            console.dir(ex)
            let message
            if(ex.response?.data?.message){
                if(message) message += ", " + ex.response.data.message
                else message = ex.response.data.message
                throw new Error(message)
            }
            else throw ex
        })
}

export const resetPassword = async (userId, token, password) => {

    let payload = {userId: userId, password: password, temporaryToken: token}

    await axios.post(endPoint + "/auth/reset-password", payload)
        .catch(ex => {
            console.dir(ex)
            let message
            if(ex.response?.data?.message){
                if(message) message += ", " + ex.response.data.message
                else message = ex.response.data.message
                throw new Error(message)
            }
            else throw ex
        })
}

export const isAuthenticated = () => {
    return !!localStorage.getItem("token")
}

export const getToken = () => {
    return localStorage.getItem("token")
}

export const verifyAccount = async (userId, activationToken) => {

    const payload = {
        userId: userId,
        temporaryToken: activationToken
    }

    console.dir(payload)

    await axios.post(endPoint + "/auth/verify-account", payload)
        .catch(ex => { throw new Error(ex) })
}