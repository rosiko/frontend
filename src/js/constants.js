export const NAME_MIN_LENGTH = 1
export const NAME_MAX_LENGTH = 16
export const PASSWORD_MIN_LENGTH = 4
export const PASSWORD_MAX_LENGTH = 20

export const EMAIL_MAX_LENGTH = 200