import {EMAIL_MAX_LENGTH} from "./constants";

export function mandatoryFieldValidation(value){
    if(!value){
        throw new Error("mandatory")
    }
}

export function compareValuesValidation(value1, value2){
    if(value1!==value2){
        throw new Error("is different")
    }
}

export function lengthValidation(value, min, max){
    mandatoryFieldValidation(value)

    if(value.length < min){
        throw new Error("too short")
    }
    if(value.length > max){
        throw new Error("too long")
    }
}

export function emailValidation(email){
    const emailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/;
    lengthValidation(email, 0, EMAIL_MAX_LENGTH)

    if(!emailRegex.test(email)){
        throw new Error("not valid")
    }
}