import axios from 'axios';
import {getToken} from "./clients/authenticationClient";


const useAxiosInterceptor = (secure) => {
    return  secure ? buildSecureAxios() : buildAxios()
}

const buildSecureAxios = () => {
    const axiosInstance = axios.create();

    axiosInstance.interceptors.request.use(
        (config) => {
            const token = getToken();

            if(token) config.headers['Authorization'] = `Bearer ${token}`
            else{
                console.error("User not logged")
                window.location.href = '/login'
            }
            return config;
        }
    )

    axiosInstance.interceptors.response.use(
        (response) => response,
        (error) => {
            if (error.response && (error.response.status === 401 || error.response.status === 403)) {
                window.location.href = '/login'
            }
            return handleError(error);
        }
    )

    return axiosInstance;
}

const buildAxios = () => {
    const axiosInstance = axios.create();

    axiosInstance.interceptors.response.use(
        response => response,
        error => handleError(error)
    )

    return axiosInstance;
}

const handleError = (error) => {
    console.error(error.message)
    console.dir(error)

    if(error.response?.status){
        let statusCode = error.response.status
        if(500 <= statusCode && statusCode < 600){
            error.message = "internal server error"
        }
        else if(error.response.data?.message){
            error.message = error.response.data?.message
        }
    }

    return Promise.reject(error);
}


export default useAxiosInterceptor;
