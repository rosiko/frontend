import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import ControlPanel from "../components/ControlPanel";
import GameMap from "../components/Map";
import Mission from "../components/Mission";
import GameCard from "../components/GameCard";
import '../styles/Match.css';
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { getTheme } from "../js/armiesPalette";
import MatchController, {
    getMatch,
    getPlayer,
    getUserId,
    placeArmies,
    selectAttacker,
    selectDefender
} from '../js/clients/matchClient';
import endPoint from "../js/clients/endPoint";
import getTerritoryById from "../js/utils/mapUtils";
import SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs";

function Match() { 
    const navigate = useNavigate()
    const { id: matchId } = useParams()
    const [match, setMatch] = useState(null)
    const [player, setPlayer] = useState(null)
    const [armiesToPlaceThisTurn, setArmiesToPlaceThisTurn] = useState([])
    const [movedArmiesThisTurn, setMovedArmiesThisTurn] = useState(0)     //Armate mosse durante il turno
    const [userId, setUserId] = useState()

    useEffect(()=>{
        const fetchData = async () => {
            try{
                let match = await getMatch( matchId );
                let userId = await getUserId()
                setUserId(userId)
                setMatch(match)
            }
            catch( error ){
                console.error(error)
            }
        }

        fetchData();
        
    },[matchId]);

    useEffect(()=>{
        if(match && userId){
            setPlayer(getPlayer(match, userId));
        }
    },[match, userId]);

    useEffect(() => {
        const socket = new SockJS(endPoint + '/ws');
        const stompClient = Stomp.over(socket);

        stompClient.connect({}, (frame) => {
            console.log('Connected: ' + frame);
            stompClient.subscribe(`/topic/matches/${matchId}` , (message) => {
                setMatch(JSON.parse(message.body))
            });
        });

        return () => {
            if (stompClient) {
                stompClient.disconnect();
            }
        };
    }, []);


    const onClickHandler = (e) => {
        if( !e.target.className.animVal.includes("territory")) return

        let territories = match.map.territories;
        let territoryId = e.target.parentElement.id;
        
        if(canPlayerPlaceArmies()){ 
            placeArmyHandler( territoryId ) 
            return
        }

        //Fase di attacco durante il proprio turno
        if(match.stage === "ATTACK" && match.playerOnDutyId === player.id){
            
            for(let territory of territories){
                //Selezione del territorio attaccante
                if(territory.id === territoryId && territory.ownerId === player.id && territory.isSelectable === true){
                    selectAttacker(match.id, territory.id)
                    break
                }

                //Selezione del territorio difensore
                if(territory.id === territoryId && !territory.ownerId !== player.id && territory.isSelectable === true && match.attack.attackerId !== null){
                    selectDefender(match.id, territory.id)
                    break
                }
            }
        }

        //Fase di spostamento delle armate
        if(match.stage === "DISPLACEMENT" && match.playerOnDutyId === player.id){
            
            for(let territory of territories){
                //Selezione del territorio da cui spostare le armate
                if(territory.id === territoryId && territory.ownerId === player.id && territory.isSelectable === true && match.armiesMovement.territoryFromId === null){
                    try{MatchController.selectTerritoryFrom(match, territory, setMatch);}
                    catch(e){
                        console.error(e);
                        navigate("/"); 
                    }
                    break;
                }

                //Selezione del territorio su cui spostare le armate
                if(territory.id === territoryId && territory.ownerId === player.id && territory.isSelectable === true && match.armiesMovement.territoryFromId !== null){
                    try{MatchController.selectTerritoryTo(match, territory, setMatch);}
                    catch(e){
                        console.error(e);
                        navigate("/"); 
                    }
                    break;
                }
            }
        } 
    }
    
    const onMouseOverHandler = (e) => {        
        if(player.id === match.playerOnDutyId){
            let territoryId = e.target.parentElement.id;        
            for(let territory of match.map.territories){
                if(territory.id === territoryId && territory.isSelectable === true){
                    e.target.parentElement.children[0].style.opacity = "0.6";
                    e.target.parentElement.style.cursor = "pointer";
                    break;
                }
            }
        }        
    }

    const onMouseOutHandler = (e) => {        
        if(e.target.className.animVal.includes("territory")) {   
            e.target.parentElement.children[0].style.opacity = '1';   
            e.target.parentElement.style.cursor = "default";
        }
    }    

    const deselectCards = (newCards) => {
        for(let i=0; i<newCards.length; i++){ newCards[i].selected = false; }
    } 

    const selectCard = (id) => {
        let newCards = JSON.parse(JSON.stringify(player.cards));
        let selectedCards = 0;

        //Conta quante carte sono selezionate esclusa l'ultima
        for(let i=0; i<newCards.length; i++){
            if(newCards[i].selected === true) selectedCards++;
        }

        for(let i=0; i<newCards.length; i++){
            if(newCards[i].id === id){
                //Se la carta è già selezionata allora deselezionale tutte
                if(newCards[i].selected === true) deselectCards(newCards);
                else {
                    //se la carta non è selezionata selezionala, se c'erano già tre carte selezionate quelle vengono deselezionate
                    if(selectedCards === 3) deselectCards(newCards);
                    newCards[i].selected = true;                    
                }             
            }            
        }   
        
        player.cards = newCards;
        setPlayer(JSON.parse(JSON.stringify(player)));
    } 

    const getGameCards = (cards) => {
        let cardList = [];
        let component = null;

        for(let i=0; i<cards.length; i++){
            cardList[i] = (
                <GameCard 
                    key={cards[i].id}
                    className="gameCard" 
                    onClick = {() => selectCard(cards[i].id)}
                    card={cards[i]}
                    player={player}
                    territories = {match.map.territories}>
                </GameCard>
            )
        }
    
        if(cardList.length>0){
            component = <div className="cardList"> {cardList} </div>
        }

        return component;
    } 

    const canPlayerPlaceArmies = () => {

        if(player === null) return false

        let response = (
            player.id === match.playerOnDutyId
            && player.availableArmies > 0
            && (
                (match.stage === "INITIAL_PLACEMENT" && Number(player.armiesPlacedThisTurn) < 3) 
                || match.stage === "PLACEMENT"
            )
        )

        console.log("canPlayerPlaceArmies? " + player.armiesPlacedThisTurn, player.availableArmies + " response: " + response)

        return response
    }

    const placeArmyHandler = ( territoryId ) => {

        console.log("Place one army in " + territoryId)

        let territory = getTerritoryById( match.map, territoryId)

        console.dir(territory)

        if(territory.ownerId === player.id){
            
            player.armiesPlacedThisTurn++
            player.availableArmies--
            setPlayer(JSON.parse(JSON.stringify(player)))
            placeOneArmy( territoryId )

            if(!canPlayerPlaceArmies() && !(armiesToPlaceThisTurn === null)){
                console.log("You can't place no more armies")
                placeArmies(matchId, armiesToPlaceThisTurn, setArmiesToPlaceThisTurn)
            }
            else{
                console.log("You can still place armies")
            }
        }
    }

    const placeOneArmy = ( territoryId ) => {    
        let territoryAlreadyPresent = false
        let newArmiesToPlaceThisTurn = armiesToPlaceThisTurn

        if(newArmiesToPlaceThisTurn === undefined) newArmiesToPlaceThisTurn = []

        console.dir(newArmiesToPlaceThisTurn);
        console.dir(newArmiesToPlaceThisTurn.armiesToPlace)

        for(let armiesToPlace of newArmiesToPlaceThisTurn){
            if(armiesToPlace.territoryId === territoryId){
                armiesToPlace.quantity++
                territoryAlreadyPresent = true
            }
        }
        if(!territoryAlreadyPresent){
            newArmiesToPlaceThisTurn.push({"territoryId": territoryId, "quantity": 1})
        }

        console.dir(newArmiesToPlaceThisTurn)
        console.log(player.armiesPlacedThisTurn)
        setArmiesToPlaceThisTurn(JSON.parse(JSON.stringify(newArmiesToPlaceThisTurn)))
    }

    const getMatchComponent = () => {

        if (match === undefined || match === null || JSON.stringify(match) === '{}' || JSON.stringify(player) === '{}') {
            console.log("Match is null")
            return null;
        }
        if (player === null) {
            console.log("Player is null")
            return null;
        }

        return (
            <div className="match">
                <div className="mission_div">
                    <Mission
                        className="mission"
                        mission={player.mission}
                    />
                </div>
                {getGameCards(player.cards)}
                <GameMap
                    className="map full-menu"
                    match={match}
                    player={player}
                    placedarmies={armiesToPlaceThisTurn}
                    movedarmies={movedArmiesThisTurn}
                    onClick={onClickHandler}
                    onMouseOver={onMouseOverHandler}
                    onMouseOut={onMouseOutHandler}
                />
                <ControlPanel
                    match={match}
                    player={player}
                    cards={player.cards}
                    setMatch={setMatch}
                    movedArmies={movedArmiesThisTurn}
                    setMovedArmies={setMovedArmiesThisTurn}/>
            </div>
        )

    }

    console.dir(match)

    return (
        <ThemeProvider theme = {createTheme(getTheme(player))}>
            {getMatchComponent()}
        </ThemeProvider>             
    );    
}

export default Match;
