import {
    Alert,
    Button,
    Divider,
    InputAdornment,
    MenuItem,
    TextField,
    Typography
} from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import '../styles/Login.css';
import React, {useEffect, useState} from 'react';
import {
    EMAIL_MAX_LENGTH,
    NAME_MAX_LENGTH,
    NAME_MIN_LENGTH,
    PASSWORD_MAX_LENGTH,
    PASSWORD_MIN_LENGTH
} from "../js/constants";
import {getUser, updateUser} from "../js/clients/userClient";
import LanguageIcon from '@mui/icons-material/Language';
import {
    compareValuesValidation,
    mandatoryFieldValidation,
    lengthValidation
} from "../js/dataValidations";

function Settings() {
    const [user, setUser] = useState()

    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [newPassword, setNewPassword] = useState()
    const [confirmNewPassword, setConfirmNewPassword] = useState()

    const [nameError, setNameError] = useState()
    const [newPasswordError, setNewPasswordError] = useState()
    const [confirmNewPasswordError, setConfirmNewPasswordError] = useState()
    const [responseError, setResponseError] = useState()
    const [responseMessage, setResponseMessage] = useState()

    useEffect(()=> fetchUser(),[])

    const fetchUser = async () => {
        let user = await getUser();
        console.log("User: ")
        console.dir(user)
        setUser(user)
        setEmail(user.email)
        setName(user.name)
        setNewPassword(null)
        setConfirmNewPassword(null)
    }


    const handleName = event => {
        setName(event.target.value)
        setNameError(null)
    }

    const handleNewPassword = event => {
        setNewPassword(event.target.value)
        setNewPasswordError(null)
    }

    const handleConfirmNewPassword = event => {
        setConfirmNewPassword(event.target.value)
        setConfirmNewPasswordError(null)
    }

    const isFormCorrect = () => {
        let isFormCorrect = true

        try{
            lengthValidation(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH)
            setNameError(null)
        }
        catch (error) {
            isFormCorrect = false
            setNameError(error.message)
        }

        if(newPassword){
            try{
                lengthValidation(newPassword, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH)
                setNewPasswordError(null)

                try{
                    mandatoryFieldValidation(confirmNewPassword)
                    compareValuesValidation(newPassword, confirmNewPassword)
                    setConfirmNewPasswordError(null)
                }
                catch (error) {
                    isFormCorrect = false
                    setConfirmNewPasswordError(error.message)
                }
            }
            catch (error) {
                isFormCorrect = false
                setNewPasswordError(error.message)
            }
        }

        return isFormCorrect
    }

    const updateUserSubmit = async () => {
        setResponseMessage(null)
        setResponseError(null)

        if(isFormCorrect()){
            try{
                await updateUser(name, newPassword)
                fetchUser()
                setResponseMessage("Changes saved")
            }
            catch(ex) {
                setResponseError(ex.message)
            }
        }
    }

    const showError = () => {
        if(responseError){
            return (
                <div>
                    <Alert severity="error">{responseError}</Alert>
                    <br/>
                </div>
            )
        }
    }

    const showMessage = () => {
        if(responseMessage){
            return (
                <div>
                    <Alert severity="success">{responseMessage}</Alert>
                    <br/>
                </div>
            )
        }
    }

    const isUserChanged = () => {
        return !!user && name !== user.name
    }

    const isPasswordChanged = () => {
        return user && !!newPassword && !!confirmNewPassword && newPassword !== user.newPassword
    }

    return (
        <div className="home">
            <div className="menu">
                <h1>Settings</h1>

                <form>
                    <Typography>Profile</Typography>

                    <TextField className="form-input" id="name" label="Name" defaultValue=" " size="small"
                               inputProps={{ maxLength: NAME_MAX_LENGTH }} error={!!nameError} helperText={nameError}
                               autoComplete="off" onChange={handleName} value={name}/>

                    <TextField className="form-input" id="email" label="Email" defaultValue=" " disabled size="small"
                               inputProps={{ maxLength: EMAIL_MAX_LENGTH }} value={email}/>

                    <TextField className="form-input" id="language" select value="EN" label="Language" disabled size="small"
                               InputProps={{
                                   startAdornment: (
                                       <InputAdornment position="start">
                                           <LanguageIcon />
                                       </InputAdornment>
                                   ),
                               }}>
                        <MenuItem value="EN">English</MenuItem>
                    </TextField>

                    <Typography>Change password</Typography>

                    <TextField className="form-input" id="new-password" label="New password" type="password"
                               autoComplete="new-password" size="small" error={!!newPasswordError}
                               helperText={newPasswordError} onChange={handleNewPassword} value={newPassword}
                               inputProps={{ maxLength: PASSWORD_MAX_LENGTH }} />

                    <TextField className="form-input" id="confirmPassword" label="Repeat password"
                               autoComplete="off" size="small" error={!!confirmNewPasswordError}
                               type="password" helperText={confirmNewPasswordError} onChange={handleConfirmNewPassword}
                               value={confirmNewPassword} inputProps={{ maxLength: PASSWORD_MAX_LENGTH }}/>

                    {showError()}
                    {showMessage()}

                    <div className="form-buttons">
                        <Button
                            className="fomr-button"
                            variant="outlined"
                            component={RouterLink}
                            to="/">
                            Back
                        </Button>
                        <Button
                            disabled={ !isUserChanged() && !isPasswordChanged()}
                            className="fomr-button"
                            variant="contained"
                            onClick={() => updateUserSubmit()}>
                            Save changes
                        </Button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Settings
