import '../styles/App.css';
import React from 'react';
import { createTheme, ThemeProvider } from "@mui/material/styles";
import AppRoutes from "../components/routes";
import globe from "../images/map_cleaned.svg"

/*import home_background from '../images/globe.svg';*/

const theme = createTheme(
    {
        palette: {
            primary: {
                main:"#4D7B5F",
                light:"#616161",
                dark: "#000000",
                contrastText: "white"
            },
            secondary: {
                main:"#fafafa",
                light:"#ffffff",
                dark: "#eeeeee",
                contrastText: "black"
            }
        },
        shape: {
            borderRadius: 16
        }
    }
)

function App() {
    /*style={{ backgroundImage: `url(${home_background})`}}*/
    return (
        <div className="background">
            <div className="App">
                <ThemeProvider theme={theme}>
                    <AppRoutes/>
                </ThemeProvider>
            </div>
        </div>
    );
}

export default App;
