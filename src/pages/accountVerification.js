import {useLocation} from 'react-router-dom';
import '../styles/Home.css';
import React, {useEffect} from 'react';
import {verifyAccount} from "../js/clients/authenticationClient";

function AccountVerification() {
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);


    useEffect(async () => {
        let userId = queryParams.get('id');
        let token = queryParams.get('token');

        console.log("Verify account with token: " + token)

        await verifyAccount(userId, token)

        window.location.href = "/login?message=Your account has been activated"
    }, [queryParams]);

    return (
        <div className="home">We are verifying your account ...</div>
    )
}

export default AccountVerification;
