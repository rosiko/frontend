import {Alert, Button, CircularProgress, TextField, Typography} from '@mui/material'
import { Link as RouterLink } from 'react-router-dom'
import React, {useState} from 'react'
import {register} from "../js/clients/authenticationClient"
import {
    compareValuesValidation,
    emailValidation,
    mandatoryFieldValidation,
    lengthValidation
} from "../js/dataValidations"
import {
    EMAIL_MAX_LENGTH,
    NAME_MAX_LENGTH,
    NAME_MIN_LENGTH,
    PASSWORD_MAX_LENGTH,
    PASSWORD_MIN_LENGTH
} from "../js/constants"

function Register() {
    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [confirmPassword, setConfirmPassword] = useState()

    const [nameError, setNameError] = useState()
    const [emailError, setEmailError] = useState()
    const [passwordError, setPasswordError] = useState()
    const [confirmPasswordError, setConfirmPasswordError] = useState()
    const [errorMessage, setErrorMessage] = useState()
    const [successMessage, setSuccessMessage] = useState()
    const [loading, setLoading] = useState(false)


    const handleName = event => {
        let value = event.target.value
        setName(value)
        setNameError(null)
    }

    const handleEmail = event => {
        setEmail(event.target.value)
        setEmailError(null)
    }

    const handlePassword = event => {
        setPassword(event.target.value)
        setPasswordError(null)
    }

    const handleConfirmPassword = event => {
        setConfirmPassword(event.target.value)
        setConfirmPasswordError(null)
    }

    const isFormCorrect = () => {
        let isFormCorrect = true
        try{
            emailValidation(email)
            setEmailError(null)
        }
        catch (error) {
            isFormCorrect = false
            setEmailError(error.message)
        }

        try{
            lengthValidation(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH)
            setNameError(null)
        }
        catch (error) {
            isFormCorrect = false
            setNameError(error.message)
        }

        try{
            lengthValidation(password, PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH)
            setPasswordError(null)
        }
        catch (error) {
            isFormCorrect = false
            setPasswordError(error.message)
        }

        try{
            mandatoryFieldValidation(confirmPassword)
            compareValuesValidation(password, confirmPassword)
            setConfirmPasswordError(null)
        }
        catch (error) {
            isFormCorrect = false
            setConfirmPasswordError(error.message)
        }

        return isFormCorrect
    }

    const registrationSubmit = async () => {
        if(isFormCorrect()){
            try{
                setLoading(true)
                await register(name, email, password)
                setErrorMessage(null)
                setSuccessMessage("User successfully registered. You will receive an email for user activation")
            }
            catch(ex) {
                setErrorMessage(ex.message)
                setSuccessMessage(null)
            }
            finally {
                setLoading(false)
            }
        }
    }

    const showMessage = () => {
        if(errorMessage) return <Alert className="form-message" severity="error">{errorMessage}</Alert>
        else if(successMessage) return <Alert className="form-message" severity="success">{successMessage}</Alert>
    }

    return (
        <div className="home">
            <div className="menu">
                <Typography className='menu-title' variant='h1' style={{fontFamily: "Luckiest Guy"}}>Rosiko</Typography>

                <form>
                    <div className="form-inputs form-overflow">
                        <TextField required className="form-input" id="name" label="Name" variant="outlined" size="small"
                                   inputProps={{ maxLength: NAME_MAX_LENGTH }} error={!!nameError} helperText={nameError}
                                   onChange={handleName} value={name} autoComplete="off"/>

                        <TextField required className="form-input" id="email" label="Email" variant="outlined"
                                   size="small"
                                   error={!!emailError} helperText={emailError} onChange={handleEmail} value={email}
                                   inputProps={{ maxLength: EMAIL_MAX_LENGTH }}/>

                        <TextField required className="form-input" id="password" label="Password" type="password"
                                   error={!!passwordError} helperText={passwordError} onChange={handlePassword}
                                   value={password} inputProps={{ maxLength: PASSWORD_MAX_LENGTH }}
                                   autoComplete="new-password" size="small"/>

                        <TextField required className="form-input" id="confirmPassword" label="Repeat password"
                                   error={!!confirmPasswordError} type="password" helperText={confirmPasswordError}
                                   onChange={handleConfirmPassword} value={confirmPassword} autoComplete="off"
                                   inputProps={{ maxLength: PASSWORD_MAX_LENGTH }} size="small"/>

                    </div>

                    {showMessage()}

                    <div className="form-buttons">
                        <Button
                            className="form-button"
                            variant="outlined"
                            component={RouterLink}
                            to="/login">
                            Login
                        </Button>
                        <Button className="form-button" variant="contained" disabled={!!successMessage}
                                onClick={() => registrationSubmit()} >
                            {loading ? <CircularProgress size={20} color="inherit" /> : 'Sign up'}
                        </Button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Register
