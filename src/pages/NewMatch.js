import React, { useState } from "react";
import {Alert, Button, CircularProgress, TextField} from '@mui/material';
import {useNavigate, Link as RouterLink} from 'react-router-dom';
import {newMatch, joinMatch} from '../js/clients/matchClient';
import {capitalizeFirstLetter} from "../js/utils/textUtils";
import {NAME_MAX_LENGTH, PASSWORD_MAX_LENGTH} from "../js/constants";
import tractorImage from "../images/tractor.png";


function NewMatch(){

    const [matchName, setMatchName] = useState()
    const [password, setPassword] = useState()
    const navigate = useNavigate();
    const [submitLoading, setSubmitLoading] = useState(false)
    const [errorMessage, setErrorMessage] = useState();


    const handleMatchName = event => {
        setMatchName(capitalizeFirstLetter(event.target.value))
    }

    const handlePassword = event => {
        setPassword(event.target.value)
    }

    const showMessage = () => {
        if(errorMessage){
            return <Alert className="form-message" severity="error">{errorMessage}</Alert>
        }
    }

    async function submitNewMatch(){
        setSubmitLoading(true)
        setErrorMessage(null)

        try {
            let matchId = await newMatch(matchName, password)
            if (matchId) navigate("/waiting_room/" + matchId)
        }
        catch (ex){
            setErrorMessage("An error occurred, please try again later")
        }
        finally { setSubmitLoading(false) }
    }

    return (
        <div className="home">
            <div className="menu">
                <h1>New match</h1>
                <form>
                    <div className="form-inputs">
                        <TextField required className="form-input" label="Match name" variant="outlined"
                                   autoComplete="off" value={matchName} onChange={handleMatchName}
                                   inputProps={{maxLength: NAME_MAX_LENGTH}}
                        />

                        <TextField className="form-input" disabled={true} label="Password" variant="outlined"
                                   autoComplete="off" type="password" value={password}
                                   onChange={handlePassword} inputProps={{maxLength: PASSWORD_MAX_LENGTH}}
                        />
                    </div>

                    {showMessage()}

                    <div className="form-buttons">
                        <Button className="form-button" variant="outlined" component={RouterLink} to="/">
                            Back
                        </Button>

                        <Button
                            className="form-button" variant="contained" disabled={!matchName || submitLoading}
                            onClick={() => submitNewMatch()}>
                            {submitLoading ? <CircularProgress size={20} color="inherit" /> : 'Done'}
                        </Button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default NewMatch
