import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import {Avatar, Button, Typography} from "@mui/material";
import { ArmiesTheme } from "../js/armiesPalette";
import '../styles/WaitingRoom.css';
import {getMatch, leavesMatch, startMatch} from "../js/clients/matchClient";
import endPoint from "../js/clients/endPoint";
import SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs";
import Cookies from 'js-cookie';

function WaitingRoom() {
    const [match, setMatch] = useState();
    const navigate = useNavigate();
    const { id } = useParams();
    
    useEffect(()=>{
        const fetchData = async ()=>{
            let match = await getMatch( id )
            setMatch(match)
        }

        fetchData()
            .catch(console.error)
    },[id])

    useEffect(() => {
        if( match && match.state === 'STARTED' ){
            navigate("/match/" + id)
        }
    }, [match])

    useEffect(() => {
        const socket = new SockJS(endPoint + '/ws');
        const stompClient = Stomp.over(socket);

        stompClient.connect({}, (frame) => {
            console.log('Connected: ' + frame);
            stompClient.subscribe(`/topic/matches/${id}` , (message) => {
                setMatch(JSON.parse(message.body))
            });
        });

        return () => {
            if (stompClient) {
                stompClient.disconnect();
            }
        };
    }, []);

    const getPlayers = () => {
        let players = match.players
        let missingPlayers = 6 - players.length

        if(players){
            return(
                <div className="player-list">
                    {
                        players.map((player) => (
                            <div className="player_item outlined" key={player.id}>
                                <Avatar sx={{ width: 24, height: 24, bgcolor: ArmiesTheme[player.color].main}}/>
                                <Typography>{player.name}</Typography>
                            </div>
                        ))
                    }
                    {
                        Array.from({ length: missingPlayers }).map((_, i) => (
                            //<Skeleton className="player_item" variant="rectangular"/>
                            <div className="player_item_placeholder" key={"missing_" + i}/>
                        ))
                    }
                </div>
            )
        }
    }

    const submitPlay = async () => {
        await startMatch(id);
    }

    const leavesMatchSubmit = () => {
        leavesMatch(id);
        Cookies.set("doNotResume", true, { expires: 1, path: '/' })
        navigate("/home"); 
    }

    const getWaitingRoomComponent = () => {
        if(match === null || match === undefined) return null;

        return (
            <div className="home">
                <div className="menu">
                    <h1>{match.name}</h1>
                    {getPlayers()}
                    <div className="form-buttons-central">
                        <Button className="form-button" variant="outlined"
                                onClick={() => {leavesMatchSubmit(match)}}>
                            Leaves
                        </Button>

                        <Button className="form-button" onClick={ () => {submitPlay()}} variant="contained"
                            disabled = {!(match.state === 'READY')}>
                            Play
                        </Button>
                    </div>
                </div>
            </div>        
        ); 
    }
  
    return getWaitingRoomComponent();
}

export default WaitingRoom;