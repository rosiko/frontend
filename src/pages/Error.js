import {Button, Typography} from "@mui/material";
import {Link as RouterLink} from "react-router-dom";
import React from "react";

function ErrorPage() {

    return (
        <div className="home">
            <div className="menu">
                <Typography className='menu-title' variant='h3' style={{fontWeight: "bold"}}>Wtf just happened?!</Typography>
                <br/>
                <Button className="home-button"
                        variant="outlined"
                        component={RouterLink}
                        to="/home">Home</Button>
            </div>
        </div>
    );
}

export default ErrorPage;