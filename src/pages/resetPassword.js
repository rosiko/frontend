import {Alert, Button, TextField, Typography} from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import '../styles/Login.css';
import React, {useState} from 'react';
import {resetPassword} from "../js/clients/authenticationClient";
import {useLocation} from 'react-router-dom';

function ResetPassword() {
    const [password, setPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();
    const [passwordError, setPasswordError] = useState();
    const [confirmPasswordError, setConfirmPasswordError] = useState();
    const [responseError, setResponseError] = useState()
    const minPasswordLength = 4;
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);
    const userId = queryParams.get('id');
    const token = queryParams.get('token');


    const handlePassword = event => {
        setPassword(event.target.value)
        setPasswordError(null)
    }

    const handleConfirmPassword = event => {
        setConfirmPassword(event.target.value)
        setConfirmPasswordError(null)
    }

    const resetPasswordSubmit = async () => {
        let isFormCorrect = validatePassword()

        if(isFormCorrect){
            try{
                await resetPassword(userId, token, password)
                setResponseError(null)
                window.location.href = "/login?message=The new password has been set successfully."
            }
            catch(ex) {
                setResponseError("" + ex.message)
            }
        }
    }

    const validatePassword = () =>{
        let isValid = true

        if(!password){
            setPasswordError("mandatory")
            isValid = false
        }
        else if(!confirmPassword){
            setConfirmPasswordError("mandatory")
            return false
        }
        else if(password.length < minPasswordLength){
            setPasswordError("too short")
            return false
        }
        else if(password !== confirmPassword){
            setConfirmPasswordError("is different")
            return false
        }
        else{
            setPasswordError(null)
        }
        return isValid
    }

    const showError = () => {
        return responseError ? <Alert severity="error">{responseError}</Alert> : null
    }

    return (
        <div className="home">
            <div className="menu">
                <Typography className='menu-title' variant='h1' style={{fontFamily: "Luckiest Guy"}}>Rosiko</Typography>
                <Typography>Insert your new password</Typography>
                <form className="login-form">
                    <TextField required className="login-input-text" id="password" label="Password" type="password"
                               error={!!passwordError} helperText={passwordError} onChange={handlePassword}>
                        {password}
                    </TextField>
                    <br/>
                    <TextField required className="login-input-text" id="confirmPassword" label="Repeat password"
                               error={!!confirmPasswordError} type="password" helperText={confirmPasswordError} onChange={handleConfirmPassword}>
                        {confirmPassword}
                    </TextField>
                </form>
                {showError()}
                <div className="login-buttons">
                    <Button
                        variant="outlined"
                        component={RouterLink}
                        to="/">
                        Home
                    </Button>
                    <Button
                        variant="contained"
                        onClick={() => resetPasswordSubmit()}>
                        Reset password
                    </Button>
                </div>
            </div>
        </div>
    );
}

export default ResetPassword;
