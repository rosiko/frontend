import {Alert, Button, CircularProgress, TextField, Typography} from '@mui/material';
import { Link as RouterLink } from 'react-router-dom';
import '../styles/Login.css';
import React, {useState} from 'react';
import {initiateResetPassword} from "../js/clients/authenticationClient";
import {emailValidation, mandatoryFieldValidation} from "../js/dataValidations";

function ForgotPassword() {
    const [email, setEmail] = useState()
    const [emailError, setEmailError] = useState()
    const [responseError, setResponseError] = useState()
    const [isEmailSent, setIsEmailSent] = useState(false)
    const [buttonEnabled, setButtonEnabled] = useState(true)
    const [submitLoading, setSubmitLoading] = useState(false)

    const handleEmail = event => {
        setEmail(event.target.value)
        setEmailError(null)
    }

    const isFormCorrect = () => {
        let isFormCorrect = true
        try{
            emailValidation(email)
            setEmailError(null)
        }
        catch (error) {
            isFormCorrect = false
            setEmailError(error.message)
        }

        return isFormCorrect
    }

    const resetPasswordSubmit = async () => {
        if(isFormCorrect()){
            setSubmitLoading(true)
            setResponseError(null)
            try{
                await initiateResetPassword(email)
                setIsEmailSent(true)

                setButtonEnabled(false);
                setTimeout(() => {
                    setButtonEnabled(true);
                }, 5 * 60 * 1000);
            }
            catch(ex) {
                console.dir(ex)
                setResponseError(ex.message)
            }
            finally {
                setSubmitLoading(false)
            }
        }
    }

    const showMessage = () => {
        if(responseError) return <Alert  className="form-message" severity="error">{responseError}</Alert>
        else if(isEmailSent) return <Alert  className="form-message" severity="success">
            Email sent, if you can't find it, check your spam folder.<br/>You can try to send a new email in 5 minutes
        </Alert>
    }

    return (
        <div className="home">
            <div className="menu">
                <Typography className='menu-title' variant='h1' style={{fontFamily: "Luckiest Guy"}}>Rosiko</Typography>
                <form>
                    <Typography>Forgotten your password? Enter your email address and you will receive an email
                        with instructions on how to reset it.</Typography>
                    <div className="form-inputs">
                        <TextField required className="form-input" id="email" label="Email" variant="outlined"
                                   error={!!emailError} helperText={emailError} onChange={handleEmail}>
                            {email}
                        </TextField>
                    </div>
                    {showMessage()}
                    <div className="form-buttons">
                        <Button
                            className="form-button"
                            variant="outlined" component={RouterLink} to="/">
                            Back
                        </Button>
                        <Button className="form-button" variant="contained" disabled={!buttonEnabled || submitLoading}
                                onClick={() => resetPasswordSubmit()} sx={{"width":"180px"}}>
                            {submitLoading ? <CircularProgress size={20} color="inherit" /> : 'Reset password'}
                        </Button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default ForgotPassword;
