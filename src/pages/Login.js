import {Alert, Button, CircularProgress, Link, TextField, Typography} from '@mui/material';
import {Link as RouterLink, useLocation} from 'react-router-dom';
import '../styles/Login.css';
import React, {useState} from 'react';
import {login, register} from '../js/clients/authenticationClient';
import {
  compareValuesValidation,
  emailValidation,
  lengthValidation,
  mandatoryFieldValidation
} from "../js/dataValidations";
import {NAME_MAX_LENGTH, NAME_MIN_LENGTH, PASSWORD_MAX_LENGTH, PASSWORD_MIN_LENGTH} from "../js/constants";

function Login() {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [errorMessage, setErrorMessage] = useState();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const message = queryParams.get('message');
  const [emailError, setEmailError] = useState()
  const [passwordError, setPasswordError] = useState()
  const [submitLoading, setSubmitLoading] = useState(false)


  const handleEmail = event => {
    setEmail(event.target.value)
    setEmailError(null)
  }

  const handlePassword = event => {
    setPassword(event.target.value)
    setPasswordError(null)
  }

  const isFormCorrect = () => {
    let isFormCorrect = true
    try{
      emailValidation(email)
      setEmailError(null)
    }
    catch (error) {
      isFormCorrect = false
      setEmailError(error.message)
    }

    try{
      mandatoryFieldValidation(password)
      setPasswordError(null)
    }
    catch (error) {
      isFormCorrect = false
      setPasswordError(error.message)
    }

    return isFormCorrect
  }

  const showMessage = () => {
    if(errorMessage){
      return <Alert className="form-message" severity="error">{errorMessage}</Alert>
    }
    else if (message){
      return <Alert className="form-message" severity="success">{message}</Alert>
    }
  }

  const submitLogin = async () => {
    try{
      if(isFormCorrect()){
        setSubmitLoading(true)
        await login(email, password)
        window.location.href = '/'
      }
    }
    catch(ex) {
      if(ex.message) setErrorMessage(ex.message)
      else setErrorMessage("An error occurred, please try again later")
    }
    finally {
      setSubmitLoading(false)
    }
  }

  return (
    <div className="home">
      <div className="menu">
        <Typography className='menu-title' variant='h1' style={{fontFamily: "Luckiest Guy"}}>Rosiko</Typography>

        <form>
          <div className="form-inputs">
            <TextField required className="form-input" id="user-name" label="Email" variant="outlined"
                       error={!!emailError} helperText={emailError} onChange={handleEmail}>
              {email}
            </TextField>

            <TextField required className="form-input" id="password" label="Password" type="password"
                       error={!!passwordError} helperText={passwordError} onChange={handlePassword}>
              {password}
            </TextField>

            <Typography variant="caption" className="form-input forgot-password" align="right">
              <Link className="forgot-password" component={RouterLink} to="/forgot-password">forgot password?</Link>
            </Typography>
          </div>

          {showMessage()}

          <div className="form-buttons">
            <Button
                className="form-button"
                variant="outlined"
                component={RouterLink}
                to="/register">
              Sign up
            </Button>
            <Button
                disabled={!email || !password || submitLoading}
                className="form-button"
                variant="contained"
                onClick={() => submitLogin()}>
              {submitLoading ? <CircularProgress size={20} color="inherit" /> : 'Sign in'}
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
