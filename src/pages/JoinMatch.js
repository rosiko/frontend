import React, { useState, useEffect } from "react";
import '../styles/JoinMatch.css';
import '../styles/Home.css';
import {Button, RadioGroup, Radio, CircularProgress} from "@mui/material";
import PersonIcon from '@mui/icons-material/Person';
import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import { useNavigate, Link as RouterLink } from 'react-router-dom';
import {getAvailableMatches, joinMatch} from '../js/clients/matchClient';
import SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs";
import endPoint from "../js/clients/endPoint";
import cowImage from "../images/cow.png";

const JoinMatch = () => {
    const navigate = useNavigate();
    const [matches, setMatches] = useState();
    const [matchId, setMatchId] = useState(undefined);
    const [loading, setLoading] = useState(true)

    useEffect( () => {
            const fetchData = async ()=>{
                let matches = await getAvailableMatches();
                setMatches(matches);
                setLoading(false)
            }

            fetchData()
        }, []
    )

    useEffect(() => {
        const socket = new SockJS(endPoint + '/ws');
        const stompClient = Stomp.over(socket);

        stompClient.connect({}, (frame) => {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic/available-matches', (message) => {
                setMatches(JSON.parse(message.body))
            });
        });

        return () => {
            if (stompClient) {
                stompClient.disconnect();
            }
        };
    }, []);

    const joinMatchSubmit = async ( event ) => {
        event.preventDefault();
        console.log(`Join match ${matchId}`)
        await joinMatch(matchId)
        navigate("/waiting_room/" + matchId)
    }

    const getLockIcon = (match) => {
        if(match.password) return (
            <LockIcon className='icon publicMatch' fontSize='small'/>
        )
        else return (
            <LockOpenIcon className='icon publicMatch' fontSize='small'/>
        )
    }

    const getMatches = () => {
        return(
            matches.map(match =>
                <div className="match-preview outlined" key={match.id}>
                        <Radio value={match.id} size="small"/>
                        <div className='matchLabel'>
                            <div className='matchName'>{match.name}</div>
                            <div className="match-data">
                                {getLockIcon(match)}
                                <div className="user-data">
                                    <PersonIcon className='icon' fontSize='small'/>
                                    <div>{match.players.length}</div>
                                </div>
                            </div>
                        </div>
                </div>
            )
        )
    }

    const selectMatch = (event, value) => {
        setMatchId(value);
    }

    const getPlaceholderPicture = () => {
        if(!loading) return(
            <div className="placeholder-container">
                <h3 className="placeholder-title">There are no matches :-(</h3>
                <img className="placeholder-picture" alt="cow" src={cowImage}/>
                <p className="placeholder-paragraph">
                    But you can still create a <a href="new_match">new game</a> while waiting for other players :-)
                </p>
            </div>
        )
        else return (
            <div className="placeholder-container">
                <CircularProgress className="placeholder-loading" color="inherit"/>
                <p>Loading matches ...</p>
            </div>
        )
    }

    const getMatchesRadioGroup = () => {
        if(matches?.length > 0) return (
            <div className="form-inputs form-overflow">
                <RadioGroup className="match-list" name="matcheList" value={!matchId ? matchId : ""}
                            onChange={selectMatch}>
                    {getMatches()}
                </RadioGroup>
            </div>
        )
    }


    return (
        <div className="home">
            <div className="menu">
                <h1>Join a match</h1>
                <form onSubmit={joinMatchSubmit}>
                    {matches?.length > 0 ? getMatchesRadioGroup() : getPlaceholderPicture()}
                    <div className="form-buttons">
                        <Button className="form-button" variant="outlined" component={RouterLink} to="/" >
                            Back
                        </Button>
                        <Button className="form-button" variant="contained" type="submit" disabled={!matchId}>
                            Join
                        </Button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default JoinMatch;
