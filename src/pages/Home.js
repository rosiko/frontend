import { Button, Typography } from '@mui/material';
import {Link as RouterLink, useNavigate} from 'react-router-dom';
import '../styles/Home.css';
import React, {useEffect, useState} from 'react';
import {logout} from "../js/clients/authenticationClient";
import {getCurrentMatch} from "../js/clients/userClient";
import Cookies from 'js-cookie';
import Logo from "../components/Logo";
import {useTheme} from "@mui/styles";

function Home() {
    const theme = useTheme();
    let primaryColor = theme.palette.primary.main;
    const [match, setMatch] = useState();
    const navigate = useNavigate();

    useEffect(()=>{
        let doNotResume = Cookies.get("doNotResume", false, { expires: 1, path: '/' })

        console.log("do not resume: " + doNotResume)

        if(!doNotResume){
            resumeMatch()
        }

        Cookies.remove("doNotResume", { path: '/' })
    },[])

    const resumeMatch = async () => {
        let match = await getCurrentMatch()

        if(!match) return

        let state = match?.state
        let id = match?.id

        if(state==="WAITING" || state==="READY"){
            navigate("/waiting_room/" + id)
        }
        else if(state==="STARTED"){
            navigate("/match/" + id)
        }
    }

    return (
        <div className="home">
            <div className="menu">
                <Typography className='menu-title' variant='h1' style={{fontFamily: "Luckiest Guy"}}>Rosiko</Typography>
                <Logo className="home-picture" color={"rgba(0,0,0,0.72)"}/>
                <div className="home-buttons-container">
                    <Button className="home-button" variant="contained" component={RouterLink} to="/join_match">
                        Join game
                    </Button>
                    <Button className="home-button" variant="outlined" component={RouterLink} to="/new_match">
                        New game
                    </Button>
                    <Button className="home-button" variant="outlined" component={RouterLink} to="/settings">
                        Settings
                    </Button>
                    <Button className="home-button" variant="outlined" onClick={() => {
                        logout()
                    }}
                            component={RouterLink} to="/login">
                        Logout
                    </Button>
                </div>
            </div>
        </div>
    );
}

export default Home;
